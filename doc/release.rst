Release Notes
=============

v0.5.0 -- 1/25/2025
--------------------

Changelog
+++++++++

New Features
^^^^^^^^^^^^

- Add official support for Linux
- Add experimental support for Teledyne ADQ cards with :class:`~vortex.acquire.TeledyneAcquisition`
- Add :class:`~vortex.process.CPUProcessor` for FFTW-based OCT processing
- Add :ref:`engine profiler <develop/profiler>`
- Add stack trace reporting with unhandled exceptions
- Add support for strided memory operations
- Add experimental support for GPU k-clock resampling on per A-scan basis for CUDA 11 and higher (feature ``cuda_dynamic_resampling``)

Improvements
^^^^^^^^^^^^

- Support full set of callbacks from :class:`~vortex.engine.NullEndpoint`
- Issue callbacks for endpoints in order of registration
- Change stream storage endpoints (e.g., :class:`~vortex.engine.AscanStreamEndpoint`) to honor flags and issue callbacks
- Update demos to use new and faster display widgets
- Add engine-generated markers at scan start when using leading samples
- Add stream and stack storage endpoints for all available data streams
- Avoid de-interleaving multi-channel data prior to processing
- Allow disabling of post-FFT square in OCT processors
- Support signed raw data in OCT processors in Python with :attr:`~vortex.process.CUDAProcessorConfig.interpret_as_signed`
- Allow disabling of pybind11 optimizations with ``ENABLE_PYBIND11_OPTIMIZATIONS`` CMake option to facilitate debugging
- Add accessor for device index to CUDA device tensors
- Add option to release DAQmx tasks when acquisition stops with :attr:`~vortex.io.DaqmxIo.persistent_task`
- Reorganized acquisition component namespaces for consistency (see Migration Guide)
- Allow use of Alazar cards not present in feature database
- Include sample skipping support in Alazar feature database
- Provide access to Alazar board handles with :attr:`~vortex.acquire.AlazarAcquisition.board` and :attr:`~vortex.acquire.alazar.Board.handle`
- Open file during preparation phase instead of start phase in :class:`~vortex.acquire.FileAcquisition`
- Add recycling stage to engine processing to support non-owned memory buffers
- Increase default data type for resampling index from 16-bits to 32-bits to support long spectra
- Include compile-time options (e.g., ``ENABLE_EXCEPTION_GUARDS``) in Python module features (e.g., ``__feature__``) for introspection

Build System
^^^^^^^^^^^^

- Add support for Python 3.11 and 3.12
- Add support for CUDA 11.x and 12.x using forward compatibility
- Rewrite CI build system for more robust and flexibility building of wheels
- Add ``manylinux2014_x86_64`` compliance for Linux wheels for portability
- Detect CUDA version in Python-driven builds to produce correct package suffix
- Support builds with GCC on Linux
- Pull Python setup script flags from CMake presets
- Build against oldest supported ``numpy`` for compatibility
- Fix dependency deployment and rpaths on Linux
- Add tests for memory management and tensors
- Add tests for OCT resampling and filtering
- Add CMake presets for unoptimized builds with GCC and Clang
- Remove dependency of engine on Alazar and DAQmx support
- Fix handling of compile time definitions in modular builds that produced builds errors with certain feature combinations
- Fix bug in handling of Alazar SDK version for error messages

Assorted
^^^^^^^^

- Fix bug when formatting to closed storage objects
- Improve logging for storage objects
- Fix bug that disabled support for Alazar on-board FFT
- Fix whitespace typo in *vcpkg* ports overlay that corrupted TBB port
- Fix dimension promotion bug in memory copy utilities
- Fix type safety for FFTW wrapper
- Fix delayed segment processing a volume boundaries in host tensor endpoints (e.g., :class:`~vortex.engine.SpectraStackHostTensorEndpoint`).
- Fix bug in scan change workaround that could produce infinite loop
- Change defaults of :class:`~vortex.format.FormatPlanner` to satisfy the most common use cases
- Improve robustness of C++ exception translation for Python
- Fix bug in engine utilization computation
- Fix assorted bugs in CPU processor
- Fix bug that unconditionally disabled engine plan logging

Migration Guide
+++++++++++++++

Namespace Changes
^^^^^^^^^^^^^^^^^

For C++ users, classes and enumerations for Alazar, DAQmx, IMAQ support have moved from ``vortex::acquire`` or ``vortex::io`` to ``vortex::alazar``, ``vortex::daqmx``, and ``vortex::imaq``, respectively.
For example, ``vortex::acquire::clock::internal_t`` is now ``vortex::alazar::clock::internal_t``.
This change avoids name collision with other acquisition components.

========================================= ==============================================
Prior Namespace                           New Namespace
========================================= ==============================================
``vortex::acquire::clock::internal_t``    ``vortex::acquire::alazar::clock::internal_t``
``vortex::acquire::frame_trigger_t``      ``vortex::acquire::imaq::frame_trigger_t``
``vortex::io::channel::digital_output_t`` ``vortex::daqmx::channel::digital_output_t``
...                                       ...
========================================= ==============================================

For Python users, objects for Alazar, DAQmx, IMAQ support have moved from ``vortex.acquire`` or ``vortex.io`` to ``vortex.acquire.alazar``, ``vortex.io.daqmx``, and ``vortex.acquire.imaq``.
Objects previously in the driver module (e.g., ``vortex.driver.alazar``) have merged with these new modules.
For example, ``vortex.acquire.InternalClock`` is now ``vortex.acquire.alazar.InternalClock`` and ``vortex.driver.alazar.Channel`` is now ``vortex.acquire.alazar.Channel``.
This change parallels C++ namespaces and simplifies Python imports.

========================================= ==============================================
Prior Module                              New Module
========================================= ==============================================
``vortex.acquire.InternalClock``          ``vortex.acquire.alazar.InternalClock``
``vortex.acquire.FrameTrigger``           ``vortex.acquire.imag.FrameTrigger``
``vortex.io.AnalogOutput``                ``vortex.io.daqmx.AnalogOutput``
...                                       ...
========================================= ==============================================

Functionality Changes
^^^^^^^^^^^^^^^^^^^^^

Stream storage endpoints (e.g., :class:`~vortex.engine.AscanStreamEndpoint`) now honor the flags setting of their formatter, just like stack storage endpoints (e.g., :class:`~vortex.engine.AscanStackEndpoint`).
The formatter defaults have been changed such that the original behavior is provided by default unless flags are customized.
Users who relied upon these endpoints to unconditionally store all samples should ensure that the endpoints are associated with a formatter than matches all flags (i.e., the default).
Users who do not customize flags are not affected by this change.

Configuration Defaults
^^^^^^^^^^^^^^^^^^^^^^

The default values for several configuration fields has changed as listed below.
Users who do not set these fields and desire the original behavior should explicitly assign the prior default value.
Users who already set these fields are not affected.

======================================================================================= ============================================== ===============================================
Field                                                                                   Prior Default                                  New Default
======================================================================================= ============================================== ===============================================
:data:`FormatPlannerConfig.adapt_shape <vortex.format.FormatPlannerConfig.adapt_shape>` ``True``                                       ``False``
:data:`FormatPlannerConfig.shape <vortex.format.FormatPlannerConfig.shape>`             ``(0, 0)``                                     ``(2**64-1, 2**64-1)``
:data:`SimpleStreamConfig.header <vortex.storage.SimpleStreamConfig.header>`            :data:`~vortex.storage.SimpleStreamHeader.Raw` :data:`~vortex.storage.SimpleStreamHeader.Empty`
======================================================================================= ============================================== ===============================================

v0.4.3 -- 6/30/2022
-------------------

Changelog
+++++++++

Hardware
^^^^^^^^

- Add support for NI IMAQ cards with :class:`~vortex.acquire.ImaqAcquisition`
- Improve feature detection for Alazar cards
- Add calibration step to reduce ADC noise for select Alazar cards
- Fix dual edge sampling for Alazar cards
- Add strobe generation to engine with :data:`EngineConfig.strobes <vortex.engine.EngineConfig.strobes>`
- Transition to dynamically loaded hardware support modules

Documentation
^^^^^^^^^^^^^

- Reorganization of documentation with expansion for acquisition and processing components
- Parse docstrings from documentation and embed in compiled module
- Add galvo delay demo and :ref:`tuning tool <how-to/io-delay>`
- Add new demo for :ref:`saving data to disk <demo/acquire-to-disk>`
- Add tutorial for :ref:`UI development and scan switching <tutorial/display>`
- Improvements and bugfixes for demos

Build System
^^^^^^^^^^^^

- Drop builds for Python 3.6
- Add builds for CUDA 11.6
- Refactor CMake to support modular builds
- Fix issue that led to incorrect type stub output
- Add experimental support for Linux
- Explicitly set minimum required NumPy version
- Improve version detection and handling for Alazar and Vortex

Assorted
^^^^^^^^

- Fix issue where non-preloadable acquisition components produced incorrect startup sequences, leading to loss of synchronization
- Exceptions that cause premature engine shutdown are now propagated
- Add :data:`~vortex.engine.Block.StreamIndex.Counter` stream to support correct index bookkeeping in post-processing with :class:`~vortex.engine.CounterStackEndpoint` and related endpoints.
- Add endpoints for formatting and storing streams (e.g., :class:`~vortex.engine.GalvoActualStackHostTensorEndpoint`)
- Refactor spiral scan generation
- Fix bug that prevented change of repeated scan strategies
- Add multiple inactive segment generation policies
- Add CPU and GPU memory endpoints for spectra (e.g., :class:`~vortex.engine.SpectraStackDeviceTensorEndpointUInt16`)
- Assorted internal improvements and bugfixes

Migration Guide
+++++++++++++++

No migration required.

v0.4.2 -- 3/3/2022
------------------

Changelog
+++++++++

- Update database for newer Alazar cards
- Add support for tolerating unknown Alazar cards
- Add Python ``setup.py`` script for building *vortex*, including all dependencies with *vcpkg*
- Fix missing constructor for :class:`~vortex.engine.Source`
- Include imaging depth in :class:`~vortex.engine.Source`
- Build for Python 3.10
- Improvements and bugfixes for demos

Migration Guide
+++++++++++++++

Building
^^^^^^^^

*vortex* now includes its own *vcpkg* overlay for its dependencies in ``/.vcpkg``.
If you previously used this path for *vcpkg* (as suggested by earlier build guides), you will need to relocate it.

v0.4.1 -- 1/22/2022
-------------------

Changelog
+++++++++

- Add support for Alazar FFT acquisition with :class:`~vortex.acquire.AlazarFFTAcquisition` and :class:`~vortex.process.CopyProcessor`.
  See the :ref:`demo/alazar-fft` demo for an example.
- Move latency handling from formatter to engine.
- Add missing :func:`~vortex.scan.RasterScanConfig.to_segments` method for high-level scan classes.
- Add Python stubs for autocompletion and type checking.
- Add CMake presets.
- Add :class:`~vortex.format.PositionFormatExecutor`.
- Fix issues with :class:`~vortex.acquire.FileAcquisition`.
- Add ``sample`` to marker-associated callbacks.
- Assorted bug fixes and improvements.

Migration Guide
+++++++++++++++

Name Changes
^^^^^^^^^^^^

- ``Stack`` has replaced ``Cube`` in class names.  For example, ``CubeTensorEndpointX`` is now ``StackTensorEndpointX``.  Similarly, ``CubeFormatExecutor`` is now :class:`~vortex.format.StackFormatExecutor`.

IO Leading
^^^^^^^^^^

IO delays (e.g., for galvo response) are no longer handled in post-processing via the formatter. Instead, the engine now generates leading IO signals that cancel out the IO delay.
The delay in samples is passed via :meth:`~vortex.engine.EngineConfig.add_io` in the :class:`~vortex.engine.EngineConfig`.
Multiple IO delays are possible.
Change ``fc.stream_delay_samples = round(cfg.galvo_delay * ioc_out.samples_per_second)`` to ``ec.add_io(io_out, lead_samples=round(cfg.galvo_delay * ioc_out.samples_per_second))``.
