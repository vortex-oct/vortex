.. _tutorial/display:

UI Integration
==============

.. warning::
    This document is under construction.

See `demo/tutorial/tutorial06.py <https://gitlab.com/vortex-oct/vortex/-/blob/develop/demo/tutorial/tutorial06.py>`_.

.. figure:: tutorial06.png

    User interface of the completed UI integration tutorial step.
