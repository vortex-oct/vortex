Design Custom Scan Patterns
===========================

.. warning::

    This document is under construction.

This document is coming soon.
In the meantime, please see the :class:`~vortex.scan.FreeformScan` example in `demo/scan_explorer.py <https://gitlab.com/vortex-oct/vortex/-/blob/develop/demo/scan_explorer.py>`_.
