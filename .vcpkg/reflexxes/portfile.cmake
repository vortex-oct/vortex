vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO Reflexxes/RMLTypeII
    REF 82031115fcf749f82986a9e397e7964007676254 # 1.2.7
    SHA512 d4ffa884e82fac98b3a935086cf546845f75130fae6b61a1e442c61ac4fcb0e3fb25eb46dad167daef88bc29018762efdcf0da58b63c93a43ae1f23138dafbfe
    HEAD_REF master
)

file(COPY ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt DESTINATION ${SOURCE_PATH})

vcpkg_configure_cmake(
    SOURCE_PATH ${SOURCE_PATH}
    PREFER_NINJA
    OPTIONS -DCMAKE_WINDOWS_EXPORT_ALL_SYMBOLS=ON
    OPTIONS_DEBUG -DCMAKE_DEBUG_POSTFIX=d
)

vcpkg_install_cmake()

vcpkg_fixup_cmake_targets()
file(REMOVE_RECURSE ${CURRENT_PACKAGES_DIR}/debug/share)

vcpkg_copy_pdbs()

file(
    INSTALL ${SOURCE_PATH}/LICENSE
    DESTINATION ${CURRENT_PACKAGES_DIR}/share/${PORT}
    RENAME copyright
)
