#include <vortex/driver/teledyne.hpp>
#include <vortex/util/cast.hpp>

#include <algorithm>
#include <iostream>

#include <fmt/format.h>

using namespace vortex::teledyne;

std::string vortex::teledyne::to_string(const trigger_source_t& v) {
    switch (v) {
    case trigger_source_t::port_trig: return "port TRIG";
    case trigger_source_t::port_sync: return "port SYNC";
    case trigger_source_t::port_gpio: return "port GPIOA0";
    case trigger_source_t::periodic: return "periodic";
    default:
        throw std::invalid_argument(fmt::format("invalid trigger source value: {}", cast(v)));
    }
}

std::string vortex::teledyne::to_string(const clock_generator_t& v) {
    switch (v) {
    case clock_generator_t::internal_pll: return "internal";
    case clock_generator_t::external_clock: return "external";
    default:
        throw std::invalid_argument(fmt::format("invalid clock generator value: {}", cast(v)));
    }
}

std::string vortex::teledyne::to_string(const clock_reference_source_t& v) {
    switch (v) {
    case clock_reference_source_t::internal: return "internal PLL";
    case clock_reference_source_t::port_clk: return "port CLK";
    case clock_reference_source_t::PXIE_10M: return "PXIE 10M";
    default:
        throw std::invalid_argument(fmt::format("invalid clock reference value: {}", cast(v)));
    }
}

std::string vortex::teledyne::channel_mask_to_string(uint32_t channel_mask)
{
    std::string s;
    for (size_t i = 0; i < std::numeric_limits<decltype(channel_mask)>::digits; i++) {
        auto x = channel_mask & (1 << i);
        if (x == 0) {
            continue;
        }
        if (!s.empty()) {
            s += "+";
        }
        s += std::to_string(i);
    }
    if (s.empty())
        s = "None";
    return s;
}

board_t::board_t(unsigned int board_index) {

    validate_ADQAPI_version();

    // Initialize the ADQ control unit object.
    _handle = CreateADQControlUnit();
    if (!valid()) {
        throw exception("failed to create ADQ API handle");
    }

    // Enable the ADQ error trace log.
    ADQControlUnit_EnableErrorTrace(handle(), LOG_LEVEL_INFO, ".");

    // List the available devices connected to the host computer.
    // Note: ADQ requires this to be called before ADQControlUnit_SetupDevice().
    ADQInfoListEntry* adq_list = nullptr;
    unsigned int n_devices = 0;
    if (!ADQControlUnit_ListDevices(handle(), &adq_list, &n_devices)) {
        throw exception("unable to list devices");
    }

    if (board_index >= n_devices) {
        throw exception(fmt::format("device index is out of range: {} >= {}", board_index, n_devices));
    }

    // Initial configuration of ADQ device;
    if (!ADQControlUnit_SetupDevice(handle(), board_index)) {
        throw exception(fmt::format("unable to set up device {}", board_index));
    }

    // Initialize our copy of the ADQ param structure.
    std::memset(&_adq, 0, sizeof(_adq));
    if (ADQ_InitializeParameters(handle(), board_index + 1, ADQ_PARAMETER_ID_TOP, &_adq) != sizeof(_adq))
        throw exception("unable to allocate device parameters");

    _info.parameters = _adq.constant;
    _info.board_index = board_index;
    // NOTE: device IDs in ADQ calls start at 1
    _info.device_number = board_index + 1;

#if ADQAPI_VERSION_MAJOR < 12
    // Tell the API whether to allocate transfer buffers in the host RAM.
    _adq.transfer.common.transfer_records_to_host_enabled = 1;
#else
    _adq.transfer.common.record_buffer_memory_owner = ADQ_MEMORY_OWNER_API;
#endif

    // If this parameter is set to 0 (default), transfer buffers will be allocated as independent memory
    // regions for all active channels. If the parameter is set to 1, the API will allocate nof_buffers
    // contiguous memory ranges, each corresponding to a transfer buffer index. Each contiguous memory range
    // will contain one transfer buffer for each active channel, placed back-to-back.
    // NOTE: this is required for the vortex memory layout
    _adq.transfer.common.packed_buffers_enabled = 1;

    // This flag controls whether ADQ may overwrite the content of buffers that
    // are currently in use by the application. Normally, this should be set to 1 to
    // prevent buffer data from being overwritten. However, it may be set to 0 to
    // completely avoid the possibility of an overflow.
    _adq.transfer.common.write_lock_enabled = 1;

    // Enable "data transfer" interface by setting marker_mode to ADQ_MARKER_MODE_HOST_MANUAL.
    _adq.transfer.common.marker_mode = ADQ_MARKER_MODE_HOST_MANUAL;
}

board_t::~board_t() {
    if(_started) {
        // ignore return value in destructor
        ADQ_StopDataAcquisition(handle(), info().device_number);
    }

    if(valid()) {
        DeleteADQControlUnit(_handle);
    }
}

const board_t::info_t& board_t::info() const {
    return _info;
}

void* board_t::handle() const {
    return _handle;
}

bool board_t::valid() const {
    return _handle != nullptr;
}

bool board_t::running() const {
    return _started;
}

void board_t::configure_sampling_clock(
    double sampling_frequency,
    double reference_frequency,
    ADQClockGenerator clock_generator,
    ADQReferenceClockSource reference_source,
    double delay_adjustment,
    int32_t low_jitter_mode_enabled)
{
    ADQClockSystemParameters& clock_system = _adq.constant.clock_system;
    clock_system.sampling_frequency = sampling_frequency;
    clock_system.reference_frequency = reference_frequency;
    clock_system.clock_generator = clock_generator;
    clock_system.reference_source = reference_source;
    clock_system.delay_adjustment = delay_adjustment;
    clock_system.delay_adjustment_enabled = (delay_adjustment != 0);
    clock_system.low_jitter_mode_enabled = low_jitter_mode_enabled;
}

void board_t::configure_trigger_source(
    ADQEventSource trigger_source,
    double periodic_trigger_frequency,
    int64_t horizontal_offset_samples,
    int32_t trigger_skip_factor
)
{
    _adq.event_source.periodic.frequency = periodic_trigger_frequency;
    for (int ch = 0; ch < _info.parameters.nof_channels; ++ch) {
        _adq.acquisition.channel[ch].horizontal_offset = horizontal_offset_samples;
        _adq.acquisition.channel[ch].trigger_source = trigger_source;
        _adq.acquisition.channel[ch].trigger_blocking_source = (trigger_skip_factor > 1) ? ADQ_FUNCTION_PATTERN_GENERATOR0 : ADQ_FUNCTION_INVALID;
    }

    ADQPatternGeneratorParameters& patternGenerator = _adq.function.pattern_generator[0];
    if (trigger_skip_factor > 1) {
        if (_adq.constant.nof_pattern_generators < 1) {
            throw exception(fmt::format("triggering skipping requires a pattern generator but there are none: {}", _adq.constant.nof_pattern_generators));
        }
        patternGenerator.nof_instructions = 2;
        // Instruction 0: Generator output 0 while waiting for the first trigger event.
        patternGenerator.instruction[0].op = ADQ_PATTERN_GENERATOR_OPERATION_EVENT;
        patternGenerator.instruction[0].source = trigger_source;
        patternGenerator.instruction[0].count = 1;
        patternGenerator.instruction[0].output_value = 0;
        patternGenerator.instruction[0].output_value_transition = 0;
        // Instruction 1: Generator 1 while waiting for N-1 trigger events.
        patternGenerator.instruction[1].op = ADQ_PATTERN_GENERATOR_OPERATION_EVENT;
        patternGenerator.instruction[1].source = trigger_source;
        patternGenerator.instruction[1].count = trigger_skip_factor - 1;
        patternGenerator.instruction[1].output_value = 1;
        patternGenerator.instruction[1].output_value_transition = 1;
    } else {
        // Disable pattern generator.
        patternGenerator.nof_instructions = 0;
    }
}

void board_t::configure_trigger_sync(bool enable) {
    if(enable) {

        // enable daisy chaining from the trigger input
        {
            auto& info = _adq.function.daisy_chain;
            // copy the trigger from the first channel
            info.source = _adq.acquisition.channel[_first_active_channel()].trigger_source;
            // info.edge = ADQ_EDGE_RISING;
            info.arm = ADQ_ARM_AT_ACQUISITION_START;
            info.resynchronization_enabled = true;
            info.position = 0;
        }

        // pass the daisy chain signal as a sync output
        {
            auto& info = _adq.port[ADQ_PORT_SYNC].pin[0];
            info.direction = ADQ_DIRECTION_OUT;
            info.function = ADQ_FUNCTION_DAISY_CHAIN;
            info.invert_output = 0;
            info.input_impedance = ADQ_IMPEDANCE_50_OHM;
        }

    } else {

        // disable daisy chaining
         _adq.function.daisy_chain.source = ADQ_EVENT_SOURCE_INVALID;
        // disable sync output
        _adq.port[ADQ_PORT_SYNC].pin[0].function = ADQ_FUNCTION_INVALID;

    }
}

#define FWOCT_REG_RECORDLENGTH  1
#define FWOCT_REG_DEBUG         2
#define FWOCT_REG_PHASEINC      9

#define FWOCT_PHASEINC_UNITY    0x10000
#define FWOCT_RECORD_DIVISOR    8

#define USER_LOGIC_2            2

void board_t::configure_resampling(double relative_phase_increment, uint32_t record_length, uint32_t channel_mapping) {
    int error;

    // set channel mappings mode
    error = ADQ_WriteUserRegister(handle(), info().device_number, USER_LOGIC_2, FWOCT_REG_DEBUG, 0, channel_mapping, nullptr);
    if(error != ADQ_EOK) {
        throw exception(fmt::format("unable to configure resampling mapping options {}: {}", channel_mapping, error));
    }

    // XXX: ensure record length is multiple of FWOCT_RECORD_DIVISOR for now
    if(record_length % FWOCT_RECORD_DIVISOR != 0) {
        throw exception(fmt::format("resampling record length of {} is not a multiple of {}", record_length, FWOCT_RECORD_DIVISOR));
    }

    // set the resampling record length
    error = ADQ_WriteUserRegister(handle(), info().device_number, USER_LOGIC_2, FWOCT_REG_RECORDLENGTH, 0, record_length, nullptr);
    if(error != ADQ_EOK) {
        throw exception(fmt::format("unable to set resampling record length to {}: {}", record_length, error));
    }

    // set the resampling record length
    auto phaseinc = uint32_t(FWOCT_PHASEINC_UNITY * relative_phase_increment);
    error = ADQ_WriteUserRegister(handle(), info().device_number, USER_LOGIC_2, FWOCT_REG_PHASEINC, 0, phaseinc, nullptr);
    if(error != ADQ_EOK) {
        throw exception(fmt::format("unable to set resampling phase increment to {} ({}): {}", relative_phase_increment, phaseinc, error));
    }
}

void board_t::configure_capture(
    uint32_t channel_mask,
    int64_t samples_per_record,
    int32_t records_per_buffer,
    int64_t records_per_acquisition,
    bool test_pattern_signal,
    int64_t sample_skip_factor)
{
    _channels.clear();

    for (uint32_t ch = 0; ch < _info.parameters.nof_channels; ++ch) {
        if (channel_mask & (1 << ch)) {
            _channels.push_back(ch);

            _adq.acquisition.channel[ch].nof_records = records_per_acquisition;
            _adq.acquisition.channel[ch].record_length = samples_per_record;

            // Reset DC offset parameter to default value (zero).
            _adq.afe.channel[ch].dc_offset = 0.0;

            // set up sample skipping
            _adq.signal_processing.sample_skip.channel[ch].skip_factor = sample_skip_factor;

            _adq.transfer.channel[ch].record_size = bytes_per_sample * samples_per_record;
#if ADQAPI_VERSION_MAJOR < 12
            _adq.transfer.channel[ch].record_length_infinite_enabled = 0;
#else
            _adq.transfer.channel[ch].infinite_record_length_enabled = 0;
#endif
            _adq.transfer.channel[ch].record_buffer_size = bytes_per_sample * records_per_buffer * samples_per_record;
            _adq.transfer.channel[ch].metadata_enabled = 0;
            _adq.transfer.channel[ch].nof_buffers = _info.buffer_count;

            // Set up test signal pattern generator.
            _adq.test_pattern.channel[ch].source = test_pattern_signal ? ADQ_TEST_PATTERN_SOURCE_TRIANGLE : ADQ_TEST_PATTERN_SOURCE_DISABLE;
        }
        else {
            // Disable recording for this channel.
            _adq.acquisition.channel[ch].nof_records = 0;
        }
    }
}

void board_t::commit_configuration() {
    std::unique_lock<std::mutex> lock(_mutex);

    if (ADQ_SetParameters(handle(), info().device_number, &_adq) != sizeof(_adq)) {
        throw exception("ADQ SetParameters() failed");
    }
}

void board_t::start_capture() {

    std::unique_lock<std::mutex> lock(_mutex);

    auto result = ADQ_StartDataAcquisition(handle(), info().device_number);
    if (result != ADQ_EOK) {
        throw exception(fmt::format("start acquisition failed: {}", result));
    }

    _started = true;
}

void board_t::stop_capture() {
    std::unique_lock<std::mutex> lock(_mutex);

    auto result = ADQ_StopDataAcquisition(handle(), info().device_number);
    _started = false;

    if (result != ADQ_EOK && result != ADQ_EINTERRUPTED) {
        throw exception(fmt::format("stop acquisition failed: {}", result));
    }
}

size_t board_t::wait_and_copy_buffer(void* ptr, size_t index, const std::chrono::milliseconds& timeout) {
    std::unique_lock<std::mutex> lock(_mutex);

    // wait for buffers to complete and lock them
    void* src;
    auto record_count = _wait_and_lock_buffer(&src, index, timeout);

    // copy all records and channels to the user buffer
    // NOTE: setting packed_buffers_enabled=1 above ensures that all channels are packed
    // NOTE: all channels share the same buffer size as per the acquisition configuration above
    auto& info = _adq.transfer.channel[_first_active_channel()];
    // TODO: consider faster copy methods
    std::memcpy(src, ptr, record_count * info.record_size * _channels.size() * bytes_per_sample);

    // return the channel buffers
    unlock_buffer(index);

    return record_count;
}

size_t board_t::wait_and_lock_buffer(void** ptr, size_t index, const std::chrono::milliseconds& timeout) {
    std::unique_lock<std::mutex> lock(_mutex);
    return _wait_and_lock_buffer(ptr, index, timeout);
}

size_t board_t::_wait_and_lock_buffer(void** ptr, size_t index, const std::chrono::milliseconds& timeout) {
    if(index >= _info.buffer_count) {
        throw std::runtime_error(fmt::format("index exceeds buffer count: {} > {}", index, _info.buffer_count));
    }

    // wait for all channels for this buffer to complete
    _wait_for_buffer(index, timeout);

    // extract the pointer for the first channel in the buffer
    // NOTE: setting packed_buffers_enabled=1 above ensures that all channels are packed
    // NOTE: all channels share the same buffer size as per the acquisition configuration above
    auto& info = _adq.transfer.channel[_first_active_channel()];
    *ptr = const_cast<void*>(info.record_buffer[index]);
    return info.record_buffer_size / info.record_size;
}

uint32_t board_t::_first_active_channel() {
    if(_channels.empty()) {
        throw std::runtime_error("no channels are active");
    } else {
        return *std::min_element(_channels.begin(), _channels.end());
    }
}

void board_t::_wait_for_buffer(size_t index, const std::chrono::milliseconds& timeout) {
    ADQP2pStatus status;

    auto deadline = std::chrono::steady_clock::now() + timeout;

    bool ready = false;
    while (!ready) {

        // wait up to remaining time for status update
        auto remaining = std::chrono::duration_cast<std::chrono::milliseconds>(deadline - std::chrono::steady_clock::now());
        auto result = ADQ_WaitForP2pBuffers(handle(), info().device_number, &status, downcast<uint32_t>(remaining.count()));

        // check for errors
        if (result == ADQ_EAGAIN) {
            // A timeout has occurred.

            // Check for device overflow situation.
            ADQOverflowStatus overflowStatus;
            if (ADQ_GetStatus(handle(), info().device_number, ADQ_STATUS_ID_OVERFLOW, &overflowStatus) != sizeof(overflowStatus)) {
                throw exception(fmt::format("unable to query device overflow status after a timeout for buffer {}", index));
            }
            if (overflowStatus.overflow) {
                throw buffer_overflow(fmt::format("hardware buffer overflow at buffer {}", index));
            }

            throw wait_timeout(fmt::format("acquisition timeout for buffer {}", index));
        }
        if (result != ADQ_EOK) {
            throw exception(fmt::format("unable to query buffer status: {}", result));
        }

        // check that all channels have a buffer ready
        ready = std::all_of(_channels.begin(), _channels.end(), [&](auto ch) {
            auto begin = status.channel[ch].completed_buffers;
            auto end = begin + status.channel[ch].nof_completed_buffers;
            return std::find(begin, end, index) != end;
        });
    }
}

void board_t::unlock_buffer(size_t index) const {
    if(index >= _info.buffer_count) {
        throw std::runtime_error(fmt::format("index exceeds buffer count: {} > {}", index, _info.buffer_count));
    }

    for(auto& ch : _channels) {
        auto result = ADQ_UnlockP2pBuffers(handle(), info().device_number, ch, 1llu << index);
        if (result != ADQ_EOK) {
            throw exception(fmt::format("failed to unlock buffer {} for channel {}: {}", index, ch, result));
        }
    }
}

std::vector<device_list_entry_t> vortex::teledyne::enumerate() {
    validate_ADQAPI_version();

    void* handle = CreateADQControlUnit();
    if (handle == nullptr) {
        throw exception("failed to create ADQ API handle");
    }

    // List the available devices connected to the host computer.
    ADQInfoListEntry* adq_list = nullptr;
    unsigned int n_devices = 0;
    auto result = ADQControlUnit_ListDevices(handle, &adq_list, &n_devices);

    std::vector<device_list_entry_t> devices;
    if(result) {
        devices.assign(adq_list, adq_list + n_devices);
    }

    // cleanup
    DeleteADQControlUnit(handle);

    if(!result) {
        throw exception("ADQ ListDevices() failed.");
    }

    return devices;
}

void vortex::teledyne::validate_ADQAPI_version() {
    // Validate ADQAPI version.
    auto result = ADQAPI_ValidateVersion(ADQAPI_VERSION_MAJOR, ADQAPI_VERSION_MINOR);
    if(result == 0) {
        // ADQAPI is compatible
    } else if (result == -2) {
        // ADQAPI is backward compatible
    } else if(result == -1) {
        throw exception(fmt::format("system ADQAPI version is incompatible with compiled version {}.{}", ADQAPI_VERSION_MAJOR, ADQAPI_VERSION_MINOR));
    } else {
        throw std::invalid_argument(fmt::format("unexpected value in ADQAPI verison check: {}", result));
    }
}
