#pragma once

#include <array>
#include <variant>
#include <algorithm>
#include <optional>

#include <spdlog/spdlog.h>

#include <xtensor/xnoalias.hpp>

#include <vortex/acquire/dso.hpp>

#include <vortex/driver/teledyne.hpp>

#include <vortex/memory/teledyne.hpp>

#include <vortex/util/cast.hpp>
#include <vortex/util/sync.hpp>
#include <vortex/util/thread.hpp>
#include <vortex/util/tuple.hpp>
#include <vortex/util/variant.hpp>

#include <vortex/core.hpp>

namespace vortex::teledyne {

    struct clock_t {
        size_t sampling_frequency = 2'500'000'000;
        size_t reference_frequency = 10'000'000;
        clock_generator_t clock_generator = clock_generator_t::internal_pll;
        clock_reference_source_t reference_source = clock_reference_source_t::internal;
        size_t delay_adjustment = 0;
        bool low_jitter_mode_enabled = true;
    };

    struct input_t {
        int channel = 0;
    };

}

namespace vortex::acquire {
    struct teledyne_config_t : dso_config_t {

        size_t device = 0;

        teledyne::clock_t clock;
        std::vector<teledyne::input_t> inputs;

        teledyne::trigger_source_t trigger_source = teledyne::trigger_source_t::port_trig;
        int32_t trigger_skip_factor = 1;
        int64_t trigger_offset_samples = 0;
        bool trigger_sync_passthrough = true;

        int64_t sample_skip_factor = 1;

        double resampling_factor = 0;
        uint32_t resampling_mapping = 0;

        std::chrono::milliseconds acquire_timeout = std::chrono::seconds(1);

        bool stop_on_error = true;

        double periodic_trigger_frequency = 10'000; // Hz
        bool test_pattern_signal = false;

        void validate() {
            // require at least one channel
            if (inputs.empty()) {
                throw std::runtime_error("no inputs are configured");
            }

            // ensure channels are unique
            for (size_t i = 0; i < inputs.size(); i++) {
                for (size_t j = 0; j < i; j++) {
                    if (inputs[i].channel == inputs[j].channel) {
                        throw std::runtime_error(fmt::format("cannot configure the same channel as two different inputs: inputs[{}] = Channel #{} and inputs[{}] = Channel #{}", i, inputs[i].channel, j, inputs[j].channel));
                    }
                }
            }

            if (sample_skip_factor < 1) {
                throw std::runtime_error("sample skip factor must be positive");
            }
            if (trigger_skip_factor < 1) {
                throw std::runtime_error("trigger skip factor must be positive");
            }
        }

        uint32_t channel_mask() const {
            uint32_t mask = 0;
            for (const auto& input : inputs) {
                mask |= (1 << input.channel);
            }
            return mask;
        }

        size_t channels_per_sample() const override {
            return inputs.size();
        }

        std::array<ptrdiff_t, 3> stride() const override {
            // stride for non-interleaved channels
            return  { downcast<ptrdiff_t>(_samples_per_record), 1, downcast<ptrdiff_t>(_records_per_block * _samples_per_record) };
        }

        auto& samples_per_second() { return clock.sampling_frequency; }
        const auto& samples_per_second() const { return clock.sampling_frequency; }
    };

    class teledyne_acquisition_t {
    public:

        using output_element_t = uint16_t;
        using callback_t = std::function<void(size_t, std::exception_ptr)>;

    public:

        teledyne_acquisition_t(std::shared_ptr<spdlog::logger> log = nullptr)
            : _log(std::move(log)) {}

        virtual ~teledyne_acquisition_t() {
            stop();

            if (_pool) {
                _pool->wait_finish();
            }
        }

        const teledyne_config_t& config() const {
            return _config;
        }

        const std::optional<teledyne::board_t>& board() const {
            return _board;
        }

        virtual void initialize(teledyne_config_t config) {
            if (_log) { _log->debug("initializing Teledyne ADQ acquisition"); }

            // validate and accept the configuration
            config.validate();
            std::swap(_config, config);

            // create the digitizer board
            _board.emplace(_config.device);
            if(_log) { _log->info("using {}{} ({}) with firmware {}", _board->info().parameters.product_name, _board->info().parameters.product_options, _board->info().parameters.serial_number, _board->info().parameters.firmware.name); }

            // configure sampling clock
            if (_log) { _log->debug("configuring {} sampling clock from {} at {} samples/second with delay of {}", to_string(_config.clock.clock_generator), to_string(_config.clock.reference_source), _config.clock.sampling_frequency, _config.clock.delay_adjustment); }
            _board->configure_sampling_clock(
                _config.clock.sampling_frequency,
                _config.clock.reference_frequency,
                static_cast<ADQClockGenerator>(_config.clock.clock_generator),
                static_cast<ADQReferenceClockSource>(_config.clock.reference_source),
                _config.clock.delay_adjustment,
                _config.clock.low_jitter_mode_enabled
            );

            // configure triggering
            if (_log) { _log->debug("configuring trigger from {} with offset of {} samples and skip factor of {}", to_string(_config.trigger_source), _config.trigger_offset_samples, _config.trigger_skip_factor); }
            _board->configure_trigger_source(
                static_cast<ADQEventSource>(_config.trigger_source),
                _config.periodic_trigger_frequency,
                _config.trigger_offset_samples,
                _config.trigger_skip_factor
            );

            // prepare asynchronous acquisition
            if (_log) { _log->debug("configuring acquisition on channel(s) {} with {} samples/record, {} records/block, and sample skipping of {} (test pattern {})", teledyne::channel_mask_to_string(_config.channel_mask()), _config.samples_per_record(), _config.records_per_block(), _config.sample_skip_factor, _config.test_pattern_signal ? "on": "off"); }
            _board->configure_capture(
                _config.channel_mask(),
                _config.samples_per_record(),
                _config.records_per_block(),
                teledyne::infinite_acquisition,
                _config.test_pattern_signal,
                _config.sample_skip_factor
            );

            // NOTE: must configure sync passthrough after channels are configured since the trigger for the first channel is passed
            if(_config.trigger_sync_passthrough) {
                if (_log) { _log->debug("configuring trigger to sync passthrough"); }
                _board->configure_trigger_sync(true);
            }

            // commit configuration
            if (_log) { _log->debug("commiting digitizer board configuration"); }
            _board->commit_configuration();

            // configure resampling after configuration committed
            if(_config.resampling_factor > 0) {
                if (_log) { _log->debug("configuring OCT resampling by factor of {} with channel mapping {}", _config.resampling_factor, _config.resampling_mapping); }
                _board->configure_resampling(1 / _config.resampling_factor, _config.samples_per_record(), _config.resampling_mapping);
            }

            // launch worker pool
            _pool.emplace("Teledyne Worker", 1, [](size_t) { setup_realtime(); }, _log);
        }

    public:

        virtual void prepare() {

        }

        void start() {
            std::unique_lock<std::mutex> lock(_mutex);

            if (!running()) {
                if (_log) { _log->info("starting acquisition"); }
                _buffer_index = 0;
                _board->start_capture();
            }
        }

        void stop() {
            _stop(false);
        }

        bool running() const {
            return _board && _board->running();
        }

        template<typename V>
        size_t next(const cpu_viewable<V>& buffer) {
            return next(0, buffer);
        }
        template<typename V>
        size_t next(size_t id, const cpu_viewable<V>& buffer_) {
            const auto& buffer = buffer_.derived_cast();
            std::unique_lock<std::mutex> lock(_mutex);

            // wait for buffer
            auto [n, error] = _wait_block(id, buffer, true);
            if (error) {
                std::rethrow_exception(error);
            }

            return n;
        }

        template<typename V>
        void next_async(const cpu_viewable<V>& buffer, callback_t&& callback) {
            next_async(0, buffer, std::forward<callback_t>(callback));
        }
        template<typename V>
        void next_async(size_t id, const cpu_viewable<V>& buffer_, callback_t&& callback) {
            const auto& buffer = buffer_.derived_cast();
            std::unique_lock<std::mutex> lock(_mutex);

            _pool->post([this, id, buffer, callback = std::forward<callback_t>(callback)]() {
                // wait for buffer
                auto [n, error] = _wait_block(id, buffer, false);
                std::invoke(callback, n, error);
            });
        }

        template<typename V>
        void next_async(size_t id, teledyne::teledyne_cpu_viewable<V>& buffer_, callback_t&& callback) {
            auto& buffer = buffer_.derived_cast();
            std::unique_lock<std::mutex> lock(_mutex);

            _pool->post([this, id, &buffer, callback = std::forward<callback_t>(callback)]() {
                // wait for buffer
                auto [n, error] = _wait_block(id, buffer, false);
                std::invoke(callback, n, error);
            });
        }
        template<typename V>
        void unlock(teledyne::teledyne_cpu_viewable<V>& buffer_) {
            auto& buffer = buffer_.derived_cast();
            std::unique_lock<std::mutex> lock(_mutex);

            // save values for later
            auto ptr = buffer.data();
            auto index = buffer.buffer_index();

            _board->unlock_buffer(buffer.buffer_index());
            buffer.unbind();

            if (_log) { _log->trace("released zero-copy buffer {} / {}", index, static_cast<void*>(ptr)); }
        }

    protected:

        template<typename V>
        auto _wait_block(size_t id, V& output_buffer, bool lock_is_held) {
            // default to no records acquired
            size_t n = 0;
            std::exception_ptr error;

            // handle early abort
            if (_abort) {
                if (_log) { _log->trace("aborted block {}", id); }
                return std::make_tuple(n, error);
            }

            try {
                // check that buffers are appropriate shape
                if (!shape_is_compatible(output_buffer.shape(), _config.shape())) {
                    throw std::runtime_error(fmt::format("stream shape is not compatible with configured shape: {} !~= {}", shape_to_string(output_buffer.shape()), shape_to_string(_config.shape())));
                }

                // wait until next job is done
                if (_log) { _log->trace("waiting for block {}", id); }

                // zero copy
                // NOTE: the driver fills the buffers in sequence
                void* ptr;
                auto idx = _buffer_index % _board->info().buffer_count;
                n = _board->wait_and_lock_buffer(&ptr, idx, _config.acquire_timeout);
                output_buffer.bind(static_cast<output_element_t*>(ptr), idx);

                // advance buffers
                _buffer_index++;
            } catch (const teledyne::exception&) {
                error = std::current_exception();
                if (_log) { _log->error("error while waiting for block {}: {}", id, to_string(error)); }
            }
            if (_log) { _log->trace("acquired block {} with {} records (zero-copy buffer {} / {})", id, n, output_buffer.buffer_index(), static_cast<void*>(output_buffer.data())); }

            // stop if necessary
            if (error && _config.stop_on_error) {
                // NOTE: call the internal _stop() because the caller may have already locked the mutex
                _stop(lock_is_held);
            };

            return std::make_tuple(n, error);
        }

        template<typename V>
        auto _wait_block(size_t id, const V& output_buffer, bool lock_is_held) {
            // default to no records acquired
            size_t n = 0;
            std::exception_ptr error;

            // handle early abort
            if (_abort) {
                if (_log) { _log->trace("aborted block {}", id); }
                return std::make_tuple(n, error);
            }

            try {
                // check that buffers are appropriate shape
                if (!shape_is_compatible(output_buffer.shape(), _config.shape())) {
                    throw std::runtime_error(fmt::format("stream shape is not compatible with configured shape: {} !~= {}", shape_to_string(output_buffer.shape()), shape_to_string(_config.shape())));
                }

                // wait until next job is done
                if (_log) { _log->trace("waiting for block {}", id); }

                // normal copy
                auto idx = _buffer_index % _board->info().buffer_count;
                n = _board->wait_and_copy_buffer(output_buffer.data(), idx, _config.acquire_timeout);

                // advance buffers
                _buffer_index++;
            } catch (const teledyne::exception&) {
                error = std::current_exception();
                if (_log) { _log->error("error while waiting for block {}: {}", id, to_string(error)); }
            }
            if (_log) { _log->trace("acquired block {} with {} records", id, n); }

            // stop if necessary
            if (error && _config.stop_on_error) {
                // NOTE: call the internal _stop() because the caller may have already locked the mutex
                _stop(lock_is_held);
            };

            return std::make_tuple(n, error);
        }

        void _stop(bool lock_is_held) {
            std::unique_lock<std::mutex> lock(_mutex, std::defer_lock);
            if (!lock_is_held) {
                lock.lock();
            }

            if (running()) {
                if (_log) { _log->info("stopping acquisition"); }
                try {
                    _board->stop_capture();
                } catch (const teledyne::exception& e) {
                    if (_log) { _log->warn("exception while stopping acquisition: {}", to_string(e)); }
                }
            }

            if (_pool) {
                // abort all pending jobs
                _abort = true;
                // clear abort flag once job queue is flushed
                _pool->post([this]() { _abort = false; });
            }
        }

        std::shared_ptr<spdlog::logger> _log;

        size_t _buffer_index = 0;
        std::optional<teledyne::board_t> _board;

        std::atomic_bool _abort = false;
        std::optional<util::worker_pool_t> _pool;
        std::mutex _mutex;

        teledyne_config_t _config;

    };

}

#if defined(VORTEX_ENABLE_ENGINE)

#include <vortex/engine/adapter.hpp>

namespace vortex::engine::bind {
    template<typename block_t>
    auto acquisition(std::shared_ptr<vortex::acquire::teledyne_acquisition_t> a) {
        using adapter = adapter<block_t>;
        auto w = acquisition<block_t>(a, base_t());

        w.stream_factory = [a]() {
            return [a]() -> typename adapter::spectra_stream_t {
                auto& cfg = a->config();

                // return a pre-sized but unbound buffer
                return sync::lockable<teledyne::teledyne_cpu_view_t<typename block_t::acquire_element_t>>(nullptr, { nullptr, nullptr }, cfg.shape(), cfg.stride(), -1);
            };
        };

        w.next_async = [a](block_t& block, typename adapter::spectra_stream_t& stream_, typename adapter::acquisition::callback_t&& callback) {
            std::visit([&](auto& stream) {
                try {
                    if constexpr (teledyne::is_teledyne_cpu_viewable<decltype(view(stream))>) {
                        a->next_async(block.id, stream, std::forward<typename adapter::acquisition::callback_t>(callback));
                    } else {
                        throw unsupported_view("only Teledyne CPU views are supported");
                    }
                } catch (const unsupported_view&) {
                    callback(0, std::current_exception());
                }
            }, stream_);
        };

        w.recycle = [a](block_t& block, typename adapter::spectra_stream_t& stream_) {
            std::visit([&](auto& stream) {
                if constexpr (teledyne::is_teledyne_cpu_viewable<decltype(view(stream))>) {
                    a->unlock(stream);
                } else {
                    throw unsupported_view("only Teledyne CPU views are supported");
                }
            }, stream_);
        };

        return w;
    }
}

#endif
